#ifndef LOCK_FREE_STACK_HPP
#define LOCK_FREE_STACK_HPP

#include <memory>
#include <atomic>
#include <iostream>
#include "Platform.hpp"

template<typename T>
class LockFreeStack
{
public:
	LockFreeStack()
		: m_node_counter(0)
		, m_counter(0)
	{
			if (reinterpret_cast<uintptr_t>(&m_head) % 16)
				std::cout << "head is not aligned to 16 bytes!"; 
	}

	~LockFreeStack()
	{
		while(pop());
	}

	void push(const T& value)
	{
		counted_node new_counted_node;
		new_counted_node.m_counter = 1; 
		new_counted_node.m_node = new node(value, *this);
		load(m_head, new_counted_node.m_node->m_next);
		std::atomic_thread_fence(std::memory_order_seq_cst);
		while (!cas(m_head, new_counted_node.m_node->m_next, new_counted_node));
	}

	std::unique_ptr<T> pop()
	{
		counted_node current_head;
		load(m_head, current_head);
		for(;;)
		{
			increment_head(current_head);
			node* const ptr = current_head.m_node;
			if (!ptr)
				return nullptr;

			if (cas(m_head, current_head, ptr->m_next))
			{
				int count_increase = current_head.m_counter - 2;
				auto result = std::move(ptr->m_value);
				if (ptr->m_internal_counter.fetch_add(count_increase) == -count_increase)
					delete ptr;
				return result;
			}
			else if (1 == ptr->m_internal_counter.fetch_sub(1))
			{
					delete ptr;
			}
		}
	}

	unsigned get_node_counter() const
	{
		return m_node_counter;
	}

private:

	struct counted_node;

	void increment_head(counted_node& node)
	{
		counted_node new_node;
		
		do
		{
			new_node = node;
			++new_node.m_counter;
		}
		while(!cas(m_head, node, new_node));
		std::atomic_thread_fence(std::memory_order_seq_cst);
		node.m_counter = new_node.m_counter;
	}

	struct alignas(16) node
	{
		node(const T& value, LockFreeStack<T>& owner)
			: m_internal_counter(0)
			, m_value(std::make_unique<T>(value))
			, m_owner(owner)
		{
			m_owner.m_node_counter.fetch_add(1, std::memory_order_relaxed);
		}

		~node()
		{
			m_owner.m_node_counter.fetch_sub(1, std::memory_order_relaxed);
		}
	
		
		counted_node m_next;
		std::atomic<int> m_internal_counter;
		std::unique_ptr<T> m_value;
		LockFreeStack<T>& m_owner;
	};

	struct alignas(16) counted_node
	{
		counted_node()
			: m_counter(0)
			, m_node(nullptr)
		{
			if (reinterpret_cast<uintptr_t>(&m_counter) % 16)
				std::cout << "counter is not aligned to 16 bytes!"; 
		}

		operator int128& ()
		{
			return reinterpret_cast<int128&>(*this);
		}

		long long m_counter;
		node* m_node;
	};

	counted_node m_head;
	std::atomic<unsigned> m_node_counter;
	std::atomic<unsigned> m_counter;
};

#endif //LOCK_FREE_STACK_HPP

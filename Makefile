# -*- Makefile -*-

CXX := g++
CXXFLAGS := -g -std=c++14 -Wall -mcx16

main: main.o
	$(CXX) $^ -o $@

main.o: main.cpp LockfreeStack.hpp Platform.hpp
	$(CXX) $(CXXFLAGS) -c $<

.PHONY: clean
clean:
	rm *.o main

#ifndef PLATFORM_HPP

#ifdef _MSC_VER
#include <Windows.h>
typedef LONGLONG int128;

bool cas(volatile int128& src, int128& cmp, const int128& with)
{
	const int128* p = &with;
	return InterlockedCompareExchange128(&src, p[1], p[0], &cmp);
}

void load(volatile int128& src, int128& dst)
{
	InterlockedCompareExchange128(&src, 0, 0, &dst);
}

#else
typedef __int128_t int128;

bool cas(volatile int128& src, int128& cmp, const int128& with)
{
	int128 result = __sync_val_compare_and_swap(&src, cmp, with);
	if (result != cmp)
	{
		cmp = result;
		return false;
	}

	return true;
}

void load(volatile int128& src, int128& dst)
{
	dst = __sync_val_compare_and_swap(&src, 0, 0);
}
#endif //_WIN32

#endif //PLATFORM_HPP

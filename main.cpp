#include <iostream>
#include "LockFreeStack.hpp"
#include <thread>

#define ITERATIONS 100000

typedef unsigned long long sum_type;
LockFreeStack<int> stack;

void writer(sum_type& sum)
{
	for (unsigned i = 0; i < ITERATIONS; ++i)
	{
		stack.push(i);
		sum += i;
	}
}

void reader(sum_type& sum)
{
	std::unique_ptr<int> result;
	while(!(result = stack.pop()));
	sum += *result;

	for (unsigned i = 0; i < 16 * ITERATIONS; ++i)
	{
		result = stack.pop();
		if (result)
			sum += *result;
	}
}

int main(int argc, char* argv[])
{
	sum_type sum1 = 0, sum2 = 0, sum3 = 0, sum4 = 0;
	sum_type sum5 = 0, sum6 = 0, sum7 = 0, sum8 = 0;

	std::thread thr1(reader, std::ref(sum1));
	std::thread thr2(reader, std::ref(sum2));
	std::thread thr3(reader, std::ref(sum3));
	std::thread thr4(reader, std::ref(sum4));
	std::thread thr5(writer, std::ref(sum5));
	std::thread thr6(writer, std::ref(sum6));
	std::thread thr7(writer, std::ref(sum7));
	std::thread thr8(writer, std::ref(sum8));

	thr1.join();
	thr2.join();
	thr3.join();
	thr4.join();
	thr5.join();
	thr6.join();
	thr7.join();
	thr8.join();

	std::cout << "reader = " << sum1 + sum2 + sum3 + sum4 << " writer = " << sum5 + sum6 + sum7 + sum8; 
	std::cout << " node counter = " << stack.get_node_counter() << std::endl;
}
